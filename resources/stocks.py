from flask_restful import Resource, reqparse
import json
import os
import requests

# Init configurations
fileDir = os.path.dirname(os.path.realpath('__file__'))
keyFilePath = os.path.join(fileDir, './configs/keys.json')
keyFilePath = os.path.abspath(os.path.realpath(keyFilePath))
endpointsFilePath = os.path.join(fileDir, './configs/endpoints.json')
endpointsFilePath = os.path.abspath(os.path.realpath(endpointsFilePath))

with open(keyFilePath, 'r') as keyFile:
    keys = json.load(keyFile)
with open(endpointsFilePath, 'r') as endpointsFile:
    endpoints = json.load(endpointsFile)


queryParser = reqparse.RequestParser()

class Stocks(Resource):
    def get(self):
        queryParser.add_argument('ticker', type=str, help='Stock ticker you are searching for')
        queryArgs = queryParser.parse_args(strict=True)

        query = {'query': queryArgs['ticker'], 'page_size': 3}
        headers = {'authorization': keys['intrinioSandbox']}
        searchResponse = requests.get(endpoints['stocks']['searchAll'], params=query, headers=headers)
        print('response: ' + searchResponse.url)
        return searchResponse.json(), searchResponse.status_code
