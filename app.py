from flask import Flask
from flask_restful import Api
from resources.stocks import Stocks

app = Flask(__name__)
api = Api(app)

api.add_resource(Stocks, '/stocks/search')

if __name__ == '__main__':
   app.run(debug=True)
